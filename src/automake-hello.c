#ifdef CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "hello.h"

int main(int argc, char *argv[])
{
	int a, b;

	a = argc > 1 ? atoi(argv[1]) : 1;
	b = argc > 2 ? atoi(argv[2]) : 1;
	printf("Hello: %d+%d=%d\n",
			a, b, add(a, b));

	return EXIT_SUCCESS;
}
